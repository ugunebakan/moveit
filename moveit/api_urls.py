from django.conf.urls import url, include
from rest_framework_jwt.views import (
        obtain_jwt_token,
        refresh_jwt_token,
        verify_jwt_token,
)
from rest_framework_swagger.views import get_swagger_view
from rest_registration.api.views import register


schema_view = get_swagger_view(title='moveIT API')

app_name = 'api_v1'

urlpatterns = [
    url(r'docs/$', schema_view),
    url(r'^offer/', include('offer.urls')),
    url(r'^account/register$', register, name='register'),
    url(r'^account/login', obtain_jwt_token),
    url(r'^account/refresh', refresh_jwt_token),
    url(r'^account/verify', verify_jwt_token),
]
