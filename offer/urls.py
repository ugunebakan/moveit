from rest_framework import routers
from offer.views import OfferViewSet


router = routers.SimpleRouter()
router.register(r'', OfferViewSet, base_name='offer')

urlpatterns = router.urls
