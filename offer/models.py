from django.db import models
from django.contrib.auth import get_user_model

# user model of the application
User = get_user_model()


class Offer(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name='User',
        help_text='User of the offer',
        related_name='offers',
    )

    address_from = models.CharField(
        verbose_name='From',
        help_text='Address from',
        max_length=255,
    )

    address_to = models.CharField(
        verbose_name='To',
        help_text='Destination address of moving',
        max_length=255,
    )

    total_distance = models.PositiveIntegerField(
        verbose_name='Total Distance',
        help_text='Total distance of moving',
    )

    living_area = models.PositiveIntegerField(
        verbose_name='Living Area',
        help_text='Living area',
    )

    box_room_area = models.PositiveIntegerField(
        verbose_name='Box room area',
        help_text='Box room area',
    )

    piano = models.BooleanField(
        verbose_name='Piano',
        help_text='Piano',
        default=False,
    )

    total_price = models.PositiveIntegerField(
        verbose_name='Total Price',
        help_text='Total price of the offer',
    )

    created_at = models.DateTimeField(
        auto_now_add=True
    )

    updated_at = models.DateTimeField(
        auto_now=True
    )

    def __str__(self):
        return "{0}'s order".format(self.user)
