import math
from dataclasses import dataclass
from typing import get_type_hints


@dataclass
class Order:
    """
    Order class is created to seperate business logic from
    the actual API structure.

    Attributes:
        total_distance (int): total distance in km
        living_area (int): total area in m2 (kvm)
        box_room_area (int): total room area in m2 (kvm)
        piano (bool): If piano is included
        address_from (str): From address field
        address_to (str): To address

    TODO:
        * Calculation of distance from the address
    """
    total_distance: int = 0
    living_area: int = 0
    box_room_area: int = 0
    piano: bool = False
    address_from: str = ''
    address_to: str = ''

    def __post_init__(self, *args, **kwargs):
        self.__validate()

    def __validate(self, *args, **kwargs):
        hints = get_type_hints(self)

        for attr_name, attr_type in hints.items():
            if attr_name == 'return':
                continue

            if not isinstance(getattr(self, attr_name), attr_type):
                raise TypeError(
                    'Argument %r is not of type %s' % (attr_name, attr_type)
                )

    def _get_piano_price(self):
        return 5000 if self.piano else 0

    def __get_distance_price(self):
        if self.total_distance < 50:
            return 1000 + (self.total_distance * 10)
        elif self.total_distance >= 50 and self.total_distance < 100:
            return 5000 + (self.total_distance * 8)
        elif self.total_distance >= 100:
            return 10000 + (self.total_distance * 7)

    def __get_number_of_cars(self):
        max_car_area = 49
        return math.ceil(
            ((self.box_room_area * 2) + (self.living_area)) / max_car_area)

    def _get_total_car_price(self):
        return self.__get_distance_price() * self.__get_number_of_cars()

    def calculate(self):
        return self._get_total_car_price() + self._get_piano_price()
