from django.contrib import admin
from offer.models import Offer


class OfferAdmin(admin.ModelAdmin):
    # custom admin panel can be populated
    # with ordering and search settings
    list_display = [
        'user',
        'total_price',
        'total_distance',
        'living_area',
        'box_room_area',
        'address_from',
        'address_to',

    ]


admin.site.register(Offer, OfferAdmin)
