from rest_framework import viewsets
from offer.models import Offer
from offer.serializers import OfferSerializer


class OfferViewSet(viewsets.ModelViewSet):
    # offers should not be updated, put and patch methods removed
    http_method_names = ['get', 'post', 'options', 'head']
    serializer_class = OfferSerializer

    def get_queryset(self):
        # filtering offers based on users
        return Offer.objects.filter(
            user=self.request.user
        )
