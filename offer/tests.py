import unittest
from offer.price_calculator import Order


class TestPriceCalculator(unittest.TestCase):

    initial_data = {
        'address_to': '',
        'address_from': '',
        'total_distance': 0,
        'box_room_area': 0,
        'living_area': 0,
        'piano': False
    }

    def test_get_piano_price(self):
        self.initial_data['piano'] = False
        order = Order(**self.initial_data)
        self.assertEqual(order._get_piano_price(), 0)

        self.initial_data['piano'] = True
        order = Order(**self.initial_data)
        self.assertEqual(order._get_piano_price(), 5000)

    def test__get_distance_price(self):
        self.initial_data['total_distance'] = 10
        order = Order(**self.initial_data)
        self.assertEqual(order._Order__get_distance_price(), 1100)

        self.initial_data['total_distance'] = 49
        order = Order(**self.initial_data)
        self.assertEqual(order._Order__get_distance_price(), 1490)

        self.initial_data['total_distance'] = 50
        order = Order(**self.initial_data)
        self.assertEqual(order._Order__get_distance_price(), 5400)

        self.initial_data['total_distance'] = 51
        order = Order(**self.initial_data)
        self.assertEqual(order._Order__get_distance_price(), 5408)

        self.initial_data['total_distance'] = 99
        order = Order(**self.initial_data)
        self.assertEqual(order._Order__get_distance_price(), 5792)

        self.initial_data['total_distance'] = 100
        order = Order(**self.initial_data)
        self.assertEqual(order._Order__get_distance_price(), 10700)

    def test__get_number_of_cars(self):
        self.initial_data['living_area'] = 49
        self.initial_data['box_room_area'] = 0
        order = Order(**self.initial_data)
        self.assertEqual(order._Order__get_number_of_cars(), 1)

        self.initial_data['living_area'] = 10
        self.initial_data['box_room_area'] = 25
        order = Order(**self.initial_data)
        self.assertEqual(order._Order__get_number_of_cars(), 2)

        self.initial_data['living_area'] = 50
        self.initial_data['box_room_area'] = 0
        order = Order(**self.initial_data)
        self.assertEqual(order._Order__get_number_of_cars(), 2)

        self.initial_data['living_area'] = 100
        self.initial_data['box_room_area'] = 0
        order = Order(**self.initial_data)
        self.assertEqual(order._Order__get_number_of_cars(), 3)

        self.initial_data['living_area'] = 150
        self.initial_data['box_room_area'] = 0
        order = Order(**self.initial_data)
        self.assertEqual(order._Order__get_number_of_cars(), 4)
