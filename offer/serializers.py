from rest_framework import serializers
from offer.models import Offer
from offer.price_calculator import Order


class OfferSerializer(serializers.ModelSerializer):
    # since total price will be calculated this field
    # is a readonly field
    total_price = serializers.ReadOnlyField()

    class Meta:
        model = Offer
        fields = [
            'id',
            'address_from',
            'address_to',
            'total_distance',
            'living_area',
            'box_room_area',
            'piano',
            'total_price',
        ]

    def create(self, validated_data):
        # input data is passed to Order class
        order = Order(**validated_data)
        # price is calculated and assinged as total_price
        validated_data['total_price'] = order.calculate()
        # request user is assigned
        validated_data['user'] = self.context.get('request').user
        # DRF create function called to create the instance
        return super(OfferSerializer, self).create(validated_data)
